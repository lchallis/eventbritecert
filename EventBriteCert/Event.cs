﻿using System;

namespace EventBriteCert
{
        public class Description
        {
            public object html { get; set; }
            public object text { get; set; }
        }

        public class End
        {
            public DateTime local { get; set; }
            public string timezone { get; set; }
            public DateTime utc { get; set; }
        }

        public class Name
        {
            public string html { get; set; }
            public string text { get; set; }
        }

        public class Start
        {
            public DateTime local { get; set; }
            public string timezone { get; set; }
            public DateTime utc { get; set; }
        }

        public class Event
        {
            public int capacity { get; set; }
            public bool capacity_is_custom { get; set; }
            public object category_id { get; set; }
            public DateTime changed { get; set; }
            public DateTime created { get; set; }
            public string currency { get; set; }
            public Description description { get; set; }
            public End end { get; set; }
            public object format_id { get; set; }
            public bool hide_end_date { get; set; }
            public bool hide_start_date { get; set; }
            public string id { get; set; }
            public bool invite_only { get; set; }
            public bool is_free { get; set; }
            public bool is_locked { get; set; }
            public bool is_reserved_seating { get; set; }
            public bool is_series { get; set; }
            public bool is_series_parent { get; set; }
            public bool listed { get; set; }
            public string locale { get; set; }
            public object logo { get; set; }
            public object logo_id { get; set; }
            public Name name { get; set; }
            public bool online_event { get; set; }
            public string organizer_id { get; set; }
            public string privacy_setting { get; set; }
            public string resource_uri { get; set; }
            public bool shareable { get; set; }
            public bool show_remaining { get; set; }
            public string source { get; set; }
            public Start start { get; set; }
            public string status { get; set; }
            public object subcategory_id { get; set; }
            public int tx_time_limit { get; set; }
            public string url { get; set; }
            public string venue_id { get; set; }
            public string version { get; set; }
        }

    }