﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBriteCert
{
    public partial class CertificateUI : Form
    {
        public CertificateUI()
        {
            InitializeComponent();
        }

        private void getEventsForUI(object sender, EventArgs e)
        {
            lstItems.Items.Clear();

            var myEvents = Program.GetMyEvents();

            foreach (Event _events in myEvents.events)
            {
                var ListItem = new ListViewItem(new[] { _events.id, _events.name.text });

                ListItem.SubItems.Add(String.Format($"{_events.id}"));
                ListItem.SubItems.Add(String.Format($"{_events.name.text}"));
                lstItems.Items.Add(ListItem);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var myEvents = Program.GetMyEvents();

            var eventIdToCertify = lstItems.SelectedItems[0].SubItems[0].Text.ToString();

            var eventNameToCertify = myEvents.events.First(z => z.id == eventIdToCertify).name.text;

            var attendeesToCertify = Program.GetEventAttendees(eventIdToCertify);

            Program.CertifyAttendees(attendeesToCertify, eventNameToCertify);
        }
    }
}
