﻿using System;
using System.Collections.Generic;

namespace EventBriteCert
{
    public class Barcode
    {
        public string barcode { get; set; }
        public DateTime changed { get; set; }
        public int checkin_type { get; set; }
        public DateTime created { get; set; }
        public string status { get; set; }
    }

    public class BasePrice
    {
        public string currency { get; set; }
        public string display { get; set; }
        public string major_value { get; set; }
        public int value { get; set; }
    }

    public class EventbriteFee
    {
        public string currency { get; set; }
        public string display { get; set; }
        public string major_value { get; set; }
        public int value { get; set; }
    }

    public class Gross
    {
        public string currency { get; set; }
        public string display { get; set; }
        public string major_value { get; set; }
        public int value { get; set; }
    }

    public class PaymentFee
    {
        public string currency { get; set; }
        public string display { get; set; }
        public string major_value { get; set; }
        public int value { get; set; }
    }

    public class Tax
    {
        public string currency { get; set; }
        public string display { get; set; }
        public string major_value { get; set; }
        public int value { get; set; }
    }

    public class Costs
    {
        public BasePrice base_price { get; set; }
        public EventbriteFee eventbrite_fee { get; set; }
        public Gross gross { get; set; }
        public PaymentFee payment_fee { get; set; }
        public Tax tax { get; set; }
    }

    public class Bill
    {
    }

    public class Home
    {
    }

    public class Ship
    {
    }

    public class Work
    {
    }

    public class Addresses
    {
        public Bill bill { get; set; }
        public Home home { get; set; }
        public Ship ship { get; set; }
        public Work work { get; set; }
    }

    public class Profile
    {
        public Addresses addresses { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string name { get; set; }
    }

    public class Attendee
    {
        public object affiliate { get; set; }
        public IList<object> answers { get; set; }
        public IList<Barcode> barcodes { get; set; }
        public bool cancelled { get; set; }
        public DateTime changed { get; set; }
        public bool checked_in { get; set; }
        public Costs costs { get; set; }
        public DateTime created { get; set; }
        public string event_id { get; set; }
        public object guestlist_id { get; set; }
        public string id { get; set; }
        public object invited_by { get; set; }
        public string order_id { get; set; }
        public Profile profile { get; set; }
        public int quantity { get; set; }
        public bool refunded { get; set; }
        public string resource_uri { get; set; }
        public string status { get; set; }
        public object team { get; set; }
        public string ticket_class_id { get; set; }
        public string ticket_class_name { get; set; }
    }

    public class Pagination
    {
        public bool has_more_items { get; set; }
        public int object_count { get; set; }
        public int page_count { get; set; }
        public int page_number { get; set; }
        public int page_size { get; set; }
    }

    public class Attendees 
    {
        public IList<Attendee> attendees { get; set; }
        public Pagination pagination { get; set; }
    }
}