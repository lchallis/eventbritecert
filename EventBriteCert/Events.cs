﻿
using EventBriteCert;

public class Events
{
	public Event[] events { get; set; }
	public Pagination pagination { get; set; }
}

public class Pagination
{
	public bool has_more_items { get; set; }
	public int object_count { get; set; }
	public int page_count { get; set; }
	public int page_number { get; set; }
	public int page_size { get; set; }
}
