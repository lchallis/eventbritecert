﻿namespace EventBriteCert
{
    partial class CertificateUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddItemsToList = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lstItems = new System.Windows.Forms.ListView();
            this.EventId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.EventName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // btnAddItemsToList
            // 
            this.btnAddItemsToList.Location = new System.Drawing.Point(81, 203);
            this.btnAddItemsToList.Name = "btnAddItemsToList";
            this.btnAddItemsToList.Size = new System.Drawing.Size(413, 23);
            this.btnAddItemsToList.TabIndex = 1;
            this.btnAddItemsToList.Text = "Get Events";
            this.btnAddItemsToList.UseVisualStyleBackColor = true;
            this.btnAddItemsToList.Click += new System.EventHandler(this.getEventsForUI);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(81, 244);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(413, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Generate Certificates";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lstItems
            // 
            this.lstItems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.EventId,
            this.EventName});
            this.lstItems.FullRowSelect = true;
            this.lstItems.Location = new System.Drawing.Point(81, 49);
            this.lstItems.Name = "lstItems";
            this.lstItems.Size = new System.Drawing.Size(413, 97);
            this.lstItems.TabIndex = 3;
            this.lstItems.UseCompatibleStateImageBehavior = false;
            this.lstItems.View = System.Windows.Forms.View.Details;
            // 
            // EventId
            // 
            this.EventId.Text = "Event Id";
            this.EventId.Width = 104;
            // 
            // EventName
            // 
            this.EventName.Text = "Event Name";
            this.EventName.Width = 305;
            // 
            // CertificateUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 493);
            this.Controls.Add(this.lstItems);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnAddItemsToList);
            this.Name = "CertificateUI";
            this.Text = "CertificateUI";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnAddItemsToList;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView lstItems;
        private System.Windows.Forms.ColumnHeader EventId;
        private System.Windows.Forms.ColumnHeader EventName;
    }
}