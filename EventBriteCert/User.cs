﻿using System.Collections.Generic;

namespace EventBriteCert
{
    public class Email
    {
        public string email { get; set; }
        public bool primary { get; set; }
        public bool verified { get; set; }
    }

    public class User
    {
        public IList<Email> emails { get; set; }
        public string first_name { get; set; }
        public string id { get; set; }
        public object image_id { get; set; }
        public bool is_public { get; set; }
        public string last_name { get; set; }
        public string name { get; set; }
    }
}

/*
{
    "emails": [
        {
            "email": "challis.lachie@gmail.com",
            "primary": true,
            "verified": false
        }
    ],
    "first_name": "Lachie",
    "id": "42911887144",
    "image_id": null,
    "is_public": false,
    "last_name": "Challis",
    "name": "Lachie Challis"
}

    */
