﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;
using RestSharp.Extensions;
using System.Net.Mail;

namespace EventBriteCert
{
    public class Program
    {
        static void Main(string[] args)
        {
            var guiForm = new CertificateUI();

            guiForm.ShowDialog();

            /*
            var EventToCertify = SelectEvent(GetMyEvents());
            var AttendeesToCertify = GetEventAttendees(EventToCertify.eventId);

            if (AttendeesToCertify.attendees != null)
            {
                CertifyAttendees(AttendeesToCertify, EventToCertify.eventName);
            }
            else
            {
                Console.WriteLine("No Attendees to certify! Press enter to exit");
                Console.ReadLine();
            }
            */
        }

        public static Events GetMyEvents()
        {
            var client = new RestClient("https://www.eventbriteapi.com/v3");

            var request = new RestRequest("/users/me/owned_events/", Method.GET);

            request.AddHeader("Authorization", "Bearer " + userDetails.OathToken);
            IRestResponse response = client.Execute(request);
            var content = response.Content;

            var myEvents = (Events)JsonConvert.DeserializeObject(content, typeof(Events));

            return myEvents;
        }

        public static (string eventId, string eventName) SelectEvent(Events events)
        {
            if (events.events != null)
            {
                List<Event> eventsMenu = events.events.Where(z => z.start.local < DateTime.Now.AddDays(30)).ToList();

                int eventCount = eventsMenu.Count();

                if (eventCount > 0)
                {
                    eventsMenu.Reverse();

                    int loopcount = 0;

                    foreach (Event _event in eventsMenu)
                    {
                        loopcount++;
                        Console.WriteLine((String.Format($"{loopcount} - {_event.name.text}")));
                    }

                    string Choice = null;

                    Choice = Console.ReadLine();

                    Int32.TryParse(Choice, out int selectedEvent);

                    string chosenEventName = eventsMenu[selectedEvent - 1].name.text;

                    Console.WriteLine((String.Format($"You chose {chosenEventName}")));
                    Console.WriteLine((String.Format($"Press Enter to generate certs for {chosenEventName}")));
                    Console.ReadLine();
                    return (eventsMenu[selectedEvent - 1].id, eventsMenu[selectedEvent - 1].name.text);
                }
                else
                {
                    Console.WriteLine("No Events in previous six months, press any key to close");
                    Console.ReadLine();
                    return ("", "");
                }
            }
            else
            {
                Console.WriteLine("Events object not returned, something went wrong.");
                Console.ReadLine();
                return ("", "");
            }
        }

        public static Attendees GetEventAttendees(string eventId)
        {
            var client = new RestClient("https://www.eventbriteapi.com/v3");

            var request = new RestRequest((String.Format($"/events/{eventId}/attendees/")), Method.GET);

            request.AddHeader("Authorization", "Bearer " + userDetails.OathToken);

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            Attendees eventAttendees = JsonConvert.DeserializeObject<Attendees>(content);

            return eventAttendees;
        }

        public static void CertifyAttendees(Attendees attendees, string eventName, bool? email =  null)
        {
            foreach (Attendee attendee in attendees.attendees.Where(z => z.checked_in == true))
            {
                pdfCert.PdfGenerator.ReplacePdfForm(attendee.profile.first_name, attendee.profile.last_name, eventName);
                if (email == true)
                {
                    // do e-mail bit here. 
                }
                else
                {

                }
            }
        }

        public static (string eventId, string eventName) GetLatestEventId()
        {
            var myEvents = GetMyEvents();

            List<Event> eventList = myEvents.events.OrderBy(z => z.start.local.ToShortDateString()).ToList();

            var LatestEventId = eventList.FirstOrDefault().id;
            var LatestEventName = eventList.FirstOrDefault().name.text;
            return (LatestEventId, LatestEventName);
        }
    }
}
