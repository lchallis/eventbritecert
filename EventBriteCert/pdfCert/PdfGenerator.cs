﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using iTextSharp.text.pdf;

namespace EventBriteCert.pdfCert
{
    public class PdfGenerator
    {
        public static void ReplacePdfForm(string firstName, string lastName, string eventName)
        {
            string inputDirectory = @"D:\Creation\code\eventbritecert\pdf\";
            string outputDirectory = @"D:\Creation\code\eventbritecert\pdf\output\";
            string inputFile = inputDirectory + "samplePdfForm6.pdf";
            string outputFile = String.Format($"{outputDirectory}{firstName}-{lastName}.pdf");

            using (var inputFileStream = new FileStream(inputFile, FileMode.Open))
            using (var outputFileStream = new FileStream(outputFile, FileMode.Create))
            {
                // open template PDF
                var pdfReader = new PdfReader(inputFileStream);

                // pdf stamper to create pdf
                var stamper = new PdfStamper(pdfReader, outputFileStream);

                var form = stamper.AcroFields;
                form.GenerateAppearances = true;
                var fieldKeys = form.Fields.Keys;

                form.SetField("Name", String.Format($"{firstName} {lastName}"));
                form.SetField("Event", eventName);

                stamper.FormFlattening = true;

                stamper.Close();
                pdfReader.Close();


            }


        }
    }
}
